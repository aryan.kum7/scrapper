<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Page;
use App\Traits\UrlTrait;


class UrlProcessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,UrlTrait;
    /**
     * Create a new job instance.
     *
     * @return void
     */
     public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		while (true) {
			
        $pages = Page::where(['isCrawled'=>'1'])->get();
		
		if($pages->count()){
			foreach($pages as $page){	
				$this->urlget($page->url);	
			}
		}
		sleep(5);		
		}
		 
    }
}
