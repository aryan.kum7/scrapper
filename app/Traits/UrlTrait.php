<?php
namespace App\Traits;

use Illuminate\Http\Request;
use App\Page;
use Goutte;
use App\Jobs\UrlProcessJob;
use App\Events\UrlUpdated;

  

trait UrlTrait {

  

    /**

     * @param Request $request

     * @return $this|false|string

     */

    public function urlget($url ) {

		$dburl = $this->checkurlindb($url);
		
		if($dburl){
		$crawler = Goutte::request('GET', $url); 
		$crawler->filter('a')->each(function ($node) use (&$url){
			
			$href  = $node->attr('href');
			if($href && $href != '/' && $href[0] != '#'){
				
				if($href[0] == '/'){
					$href = $this->getDomain($url).$href;
				}
				
				$pageCount = Page::where(['url'=>$href])->get()->count();
				if($pageCount == '0'){
					$pageid = Page::create(['url'=>$href,'isCrawled'=>'1'])->id;
					$this->eventupdate((object) ['id'=>$pageid, 'url'=>$href, 'isCrawled'=>'1']);			
								
				}	
			}
		});	
		$pageid = Page::updateOrCreate(['url'=>$url],['isCrawled'=>'0'])->id;
		$this->eventupdate((object) ['id'=>$pageid, 'url'=>$href, 'isCrawled'=>'0']);
		return true;
		}

    }
	
	public function eventupdate($data) {
		event(new UrlUpdated($data));
	}
	
	
	public function checkurlindb($url) {
		$pages = Page::where(['url'=>$url])->get();
		
		if($pages->Count()){
			if($pages[0]->isCrawled == '0'){
			return false;	
			}else{
			return true;	
			}
			
		}else{
			Page::create(['url'=>$url,'isCrawled'=>'1']);	
			return true;
			
		}
	}
	
	function getDomain($url){
		
		$pieces = parse_url($url);
		$domain = isset($pieces['host']) ? $pieces['host'] : '';
		if(preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)){
			$result = parse_url($url);
  
			return $result['scheme']."://".$regs['domain'];
		}
		
		return FALSE;
	}
	
	
	

  

}