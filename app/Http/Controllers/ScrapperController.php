<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\UrlProcessJob;
use App\Traits\UrlTrait;
use App\Page;
use App\Events\UrlUpdated;


class ScrapperController extends Controller
{
    use UrlTrait;
	
	public function  ScrapUrl(Request $request)
	{
		$regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
		$request->validate([
			'url' => 'required|regex:'.$regex,
		]);
		$url = $request->input('url');
		$this->urlget($url);
		$this->dispatch(new UrlProcessJob());
		
		echo "processing";
	
	}
	
	

	
	
}
