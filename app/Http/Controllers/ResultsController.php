<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Events\UrlUpdated;

class ResultsController extends Controller
{
    public function show(){
        $pages = Page::paginate(20);
        return view('results', compact('pages'));
    }
	
	 public function leaderboard(){

        return Page::all(['id', 'url', 'isCrawled']);
    }
}
