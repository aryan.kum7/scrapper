<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/results', 'ResultsController@show');
Route::get('/leaderboard', 'ResultsController@leaderboard');
Route::post('/scrapurl', 'ScrapperController@ScrapUrl')->name('scrapurl');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

