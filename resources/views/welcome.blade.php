<!DOCTYPE html>

<html>

<head>

    <title></title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>

    <div class="container">
        @if(Session::has('success'))

        <div class="alert alert-success">

            {{ Session::get('success') }}

            @php

                Session::forget('success');

            @endphp

        </div>

        @endif

   

        <form method="post" action="{{route('scrapurl')}}">

            {{ csrf_field() }}

            <div class="form-group">

                <label>URL:</label>

                <input type="text" name="url"  class="form-control" placeholder="">

                @if ($errors->has('url'))

                    <span class="text-danger">{{ $errors->first('url') }}</span>

                @endif

            </div>

            <div class="form-group">

                <button class="btn btn-success btn-submit">Submit</button>

            </div>

        </form>

    </div>

</body>

</html>
